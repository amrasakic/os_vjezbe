#include "automobili.hpp"
void unos(std::vector<Vozac>& vektor, int br) {
    for (int i = 0; i < br; ++i) {
      Vozac vozac;
      std::cout << "Unesite podatke o: " << i << ".vozacu: " << std::endl;
      std::cout << "Ime: ";
      std::cin >> vozac.ime_;
      std::cout << "Prezime: ";
      std::cin >> vozac.prezime_;
      std::cout << "Godine: ";
      std::cin >> vozac.godine_;
      std::cout << "Model: ";
      std::cin >> vozac.model;
      std::cout << "Boja: ";
      std::cin >> vozac.boja;
      std::cout << "Godina proizvodnje: ";
      std::cin >> vozac.godina_proizvodnje;
      std::cout << "Staz vozaca: ";
      std::cin >> vozac.staz_;
      std::cout << "Vrijeme utrke: ";
      std::cin >> vozac.vrijeme;
      vektor.push_back(vozac);
    }
  }

  void ispis(std::vector<Vozac>& vektor) {
    auto itb = vektor.begin();
    auto ite = vektor.end();

    std::cout << "Tabela rezultata: " << std::endl;
    int i = 1;
    while (itb != ite) {
      std::cout << i << ". mjesto: ";
      std::cout << itb->ime_ << " ";
      std::cout << itb->prezime_ << " ";
      std::cout << itb->godine_ << " ";
      std::cout << itb->model << " ";
      std::cout << itb->boja << " ";
      std::cout << itb->godina_proizvodnje << " ";
      std::cout << itb->staz_<< " ";
      std::cout << itb->vrijeme << " ";
      ++itb;
      ++i;
    }
  }

