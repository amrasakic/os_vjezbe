
#pragma once
#include <algorithm>
#include <iostream>
#include <vector>

class Osoba {
  protected:
  std::string ime_;
  std::string prezime_;
  int godine_;
};

class Automobil {
  protected:
  std::string model;
  std::string boja;
  int godina_proizvodnje;
};
class Vozac : public Osoba, Automobil {
  private:
  int staz_;
  Automobil vozilo;
  int vrijeme;

  friend void unos(std::vector<Vozac>& vektor,int br);
  friend void ispis(std::vector<Vozac>& vektor);

};

 void unos(std::vector<Vozac>& vektor,int br);
void ispis(std::vector<Vozac>& vektor);
